#! /home/user/anaconda3/bin/python

import time
import json
import random
import pandas as pd
import datetime
import requests
import argparse
import csv
import uuid


# FUNCTIONS #

# Reads from CSV and returns dataFrame
def read_csv():
    data_frame_local = pd.read_csv(args.fileName, sep=separator)
    return data_frame_local


# Reads the columns that are going to be sent, and puts them in an appropriate format for JSON.
def read_params():
    parameters_local = ["UUID", "TIMESTAMP"]

    columns_frame = pd.read_csv(file_name, sep=separator, header=None)
    for i in range(columns):
        parameters_local.append(columns_frame.iloc[0][i])

    return parameters_local


# Returns the number of columns that need to be read
def count_columns():
    with open(file_name) as f:
        reader = csv.reader(f, delimiter=separator, skipinitialspace=True)
        first_row = next(reader)
        num_cols = len(first_row)
    return num_cols


# Retrieves a line from the dataFrame and puts it in JSON serialized format
def retrieve_line(x):
    payload = {parameters[0]: uuid}
    print(uuid)
    payload[parameters[1]] = place_time_stamp()
    for i in range(columns):
        data = str(data_frame.iloc[x][i])
        if i == 0:
            data = datetime.datetime.strptime(data_frame.iloc[x][i], "%d/%m/%Y %H:%M").strftime("%Y-%m-%dT%H:%M")
        payload[parameters[i + 2]] = data
    return json.dumps(payload)


# Returns current timestamp in ISO 8601 format.
def place_time_stamp():
    data = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    return data


# Returns UUID depending on the -u attribute established in the arguments
def place_uuid(identifier):
    data = str(uuid.uuid5(uuid.NAMESPACE_OID, identifier))
    return data


# Loads the arguments for the program
def load_args(arguments):
    if not (isinstance(arguments.separator, str)):
        arguments.separator = ','
    arguments.uuid = place_uuid(arguments.uuid)
    return arguments.fileName, arguments.periodicity, arguments.url, arguments.separator, arguments.uuid


# MAIN #

# Lists all the arguments and puts them ready for use
parser = argparse.ArgumentParser(
    description='This program provides functions to read data stored in a CSV and sending it throught http post in '
                'the desired UDP port.')
parser.add_argument('-f', '--filename', required=True, dest='fileName', help='Path of the csv file to be used')
parser.add_argument('-p', '--periodicity', dest='periodicity', type=int, required=True, default=0,
                    help='Desired periodicity (0 to make it aperiodic)')
parser.add_argument('-u', '--url', dest='url', required=True,
                    help='Introduce wanted url with corresponding port and topic. f.e = '
                         'http://192.168.1.0:2301/topic/test')
parser.add_argument('-s', '--separator', dest='separator', required=False,
                    help='Introduce desired separator (, by default)')
parser.add_argument('-id', '--uuid', dest='uuid', required=True, help='Introduce desired name to create a uuid')

args = parser.parse_args()
file_name, periodicity, url, separator, uuid = load_args(args)

columns = count_columns()
parameters = read_params()
data_frame = read_csv()
waitValue = periodicity

# Establishes the headers
headers = {'Content-Type': 'application/vnd.kafka.json.v1+json'}

row = 0
while 1:
    json_data = retrieve_line(row)

    # Sends the HTTP POST with the given url, header and data
    if periodicity == 0:
        waitValue = random.randint(1, 10)
        print(waitValue)

    response = requests.post(url=args.url, headers=headers, data='{"records":[{"value": %s}]}' % json_data)
    print(response.text)

    row += 1
    time.sleep(waitValue)
